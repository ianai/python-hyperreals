#!/usr/bin/env python

from decimal import Decimal
import random

class hyperreal(object):
    """
    I wrote this module in an effort to bring the hyperreal number system to the python
    programming language. There are a number of good introductions to the hyperreal 
    number system available on the web, so I will assume a basic familiarity.

    Here, a hyperreal object has a list of Decimal numbers (from the decimal module). 
    This list is assumed to be ordered and follow some specific assumptions.
    (See the comment for the calc_digit function.)

    Hyperreal numbers may be instantiated in the following ways:
            -By providing the class a numerical number. i.e. Hyperreal(5.0) returns a 
            a Hyperreal with standard part 5.
            -By providing a callable function that calculates the digits of the number.
                    epsilon_1_2 = Hyperreal(lambda place: Decimal(1/2**place) ) 
                    epsilon_1_2 would correspond to [1, 1/2, 1/4, 1/8, ...]
            -By numerical operations between Hyperreal object instances. (+, -, /, * all work)

    Comparative operators:
            (The operators need to be written.)
    """
    max_digits = 30 #the default

    def __init__(self, init_val = None):
        #digits is the (list) sequence of digits
        if init_val == None:
            self.digits = [Decimal("0") for digit in range(0,self.max_digits) ]
        elif type(init_val) in [int, float]:
            self.digits = [Decimal(str(init_val)) for digit in range(0, self.max_digits)]
        elif type(init_val) in [list, tuple]:
            if len(init_val) >= self.max_digits:
                self.digits = init_val
            else:
                print("hyperreal class expected a list or tuple of length %s" % str(self.max_digits) )
        else:
            print("unsupported type of initial value passed to hyperreal")
    #I'm including separate versions of .fromReal, .fromList, and .fromStaticFunciton
    #These functions have more error handling than what's implemented in __init__
    def fromReal(init_number):
        someDigits = []
        try:
            someDigits = [Decimal(str(init_number)) for digit in range(0,hyperreal.max_digits) ]
            return(hyperreal.fromList(someDigits))
        except TypeError:
            print("fromReal expects a number or something that can be turned into a number as its sole argument.")

    def fromList(init_list):
        """This takes a single argument, a list or tuple of the digits to create a hyperreal. The digits will be converted to Decimals one by one. 
        The list must be the length of the constant self.max_digits set in the class.
        """
        if len(init_list) !=  hyperreal.max_digits:
            print("hyperreal.fromList expects an init_list of length %s" % str(self.max_digits))
        else:
            instanceDigits = [Decimal(digit) for digit in init_list]
            value = hyperreal()
            value.digits = instanceDigits
        return(value)

    def fromStaticFunction(function):
        """This takes a single argument, a static function. The function will be given one argument: the number of the digit to calculate (starting with 0). 
        The hyperreal's sequence can be thought of as: ( f(0), f(1), f(2), ...  , f(self.max_digits-1) ).
        The digits will be calculated at the creation of the hyperreal.
        """
        try:
            for digit in range(0,self.max_digits):
                self.digits[digit] = Decimal(function(digit))
            return(self)
        except TypeError:
            print("fromStaticFunction expects a single callable argument. That argument should be a function that takes the digit to calculate as input.")
    
    #str conversion
    def __str__(self):
        """Returns the result of the standardPart() function. """
        return(str( self.standardPart() ))

    #begin numerical operators +, -, *, / 

    def __add__(self, other):
        """Just adds by component.
        This function doesn't do type checking on the right side of the operation, therefore allowing more flexibility so long as both quantities support .max_digits and .digits of type integer and list, respectively.
        """
        try:
            operated_digits = []
            for digit in range(0, min(self.max_digits, other.max_digits)):
                operated_digits.append(self.digits[digit] + other.digits[digit])
            #returns a hyperreal with the calculated digits
            return hyperreal.fromList(operated_digits)
        except AttributeError:
            print("both sides of hyperreal addition must have a max_digits constant and a .digits list")
            return(hyperreal())
        
    def __mul__(self, other):
        """Multiplies by component.
            This function doesn't do type checking on the right side of the operation, therefore allowing more flexibility so long as both quantities support .max_digits and .digits of type integer and list, respectively.
        """
        try:
            operated_digits = []
            for digit in range(0, min(self.max_digits, other.max_digits)):
                operated_digits.append(self.digits[digit] * other.digits[digit])
            #returns a hyperreal with the calculated digits. 
            return hyperreal.fromList(operated_digits)
        except AttributeError:
            print("both sides of hyperreal multiplication must have a max_digits constant and a .digits list")
            

    def __sub__(self, other):    
        return(hyperreal.__add__(self, hyperreal.__mul__(hyperreal.fromReal(-1), other)) )

    def __div__(self, other):
        try:
            operated_digits = []
            for digit in range(0, min(self.max_digits, other.max_digits)):
                operated_digits.append(self.digits[digit] / other.digits[digit])
        
        except ZeroDivisionError:
            print("denominator of hyperreal division contained a zero.")
        
        except AttributeError:
            print("both sides in hyperreal division must support .max_digits and .digits list")
        
        return(hyperreal.fromList(operated_digits))
    
    def __truediv__(self, other):
        return(hyperreal.__div__(self, other))
    
    def isIncreasing(self):
        """does a simple test for strinctly increasing monotonicity"""
        value = True
        for digit in range(0, self.max_digits-1):
            if self.digits[digit] >= self.digits[digit+1] :
                value = False
        
        return(value)
        
    def isDecreasing(self):
        """similar to isIncreasing"""
        value = True
        for digit in range(0, self.max_digits-1):
            if self.digits[digit] <= self.digits[digit+1] :
                value = False
        
        return(value)

    def standardPart(self):
        """Implements a standard part of function for the hyperreals in python. Returns a Decimal.
        This will erroneously return a number (either the smallest or largest number of the sequence of digits) for infinitesimals and infinites.
        
        This is a very simple standard part function and begs to be improved. 
        """
        min_digits = min(self.digits)
        max_digits = max(self.digits)
        
        if min_digits == max_digits:
            #hyperreal is a constant sequence of digits.
            return(Decimal(min_digits))
        elif self.isIncreasing(self):
            return(max_digits)
        elif self.isDecreasing(self):
            return(min_digits)
        else:
            #sequence oscilates and therefore doesn't have a single limit point
            return(None)
            
#begin the logical comparators
    def __eq__(self, other):
        """Does a simple test. Every digit of the comparison must be equal for the two to be equal. 
        
        It may make more sense to compare standard parts - but there is already a way to do that using the hyperreal.standardPart function.
        """
        try:
            value = True
            for digit in range(0, min(self.max_digits, other.max_digits)):
                if self.digits[digit] != other.digits[digit]:
                    value = False
            return(value)
        except AttributeError:
            print("both sides of hyperreal equality test must be hyperreals")
        
    def __lt__(self, other):
        """returns true if:
            -the standard part of self is less than the standard part of the other hyperreal
            or
            -the greatest digit on the left is smaller than the lowest digit from the right
        """
        value = False
        selfStandard = self.standardPart()
        otherStandard = other.standardPart()
    
        if selfStandard is None or otherStandard is None:
        #one or both oscilate 
            max_self = max(self.digits)
            min_other = min(other.digits)
            if max_self < min_other:
            #if self's max digit is less than other's lowest digit, then self is less than other
                value = True
        
        if selfStandard < otherStandard :
            #compare standard parts
            value = True
        
        #if self.digits[self.max_digits-1] < other.digits[other.max_digits -1] :
        #compare their last digits - an approx. for a trend 
        #commented out since the standard comparison probably covers this case better
        #    value = True
        
        return(value)

    def __ne__(self, other):
        return (not self == other)
    
    def __gt__(self, other):
        return(not self <= other)
    
    def __ge__(self, other):
        return(not self < other) 
    
    #end of the logical comparisons.
        
#test the module
if __name__ == "__main__":
    print("Some uses of the module")
    a=hyperreal(5)
    b=hyperreal(7)
    print(a+b)
    print(a/b)
    print(a<b)
    print(a!=b)
    
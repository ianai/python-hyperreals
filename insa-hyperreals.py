#!/usr/bin/env python 

import decimal
Decimal = decimal.Decimal 
import random

class Hyperreal(object):
	"""
	I wrote this module in an effort to bring the hyperreal number system to the python
	programming language. There are a number of good introductions to the hyperreal 
	number system available on the web, so I will assume a basic familiarity.
	
	Here, a hyperreal object has a list of Decimal numbers (from the decimal module). 
	This list is assumed to be ordered and follow some specific assumptions.
	(See the comment for the calc_digit function.)
	
	Hyperreal numbers may be instantiated in the following ways:
		-By providing the class a numerical number. i.e. Hyperreal(5.0) returns a 
		a Hyperreal with standard part 5.
		-By providing a callable function that calculates the digits of the number.
			epsilon_1_2 = Hyperreal(lambda place: Decimal(1/2**place) ) 
			epsilon_1_2 would correspond to [1, 1/2, 1/4, 1/8, ...]
		-By numerical operations between Hyperreal object instances. (+, -, /, * all work)
	
	Comparative operators:
		(The operators need to be rewritten.)
		Currently, the Hyperreal module supports all comparative operators thusly:
		-a random subset of the two Hyperreal numbers' digits are selected for comparison.
		-a false result is reached if the comparison is false for any of the digits.
	"""

	def __init__(self, value = False, operator_efficiency=15, x_parent = None, y_parent=None):
		#initialize the hyperreal number's digits.
		self.digits =  []
		self.max_digits = 30
		self.operator_efficiency = operator_efficiency
		
		if x_parent != None:
			self.x_parent = x_parent
		
		if y_parent != None:
			self.y_parent = y_parent
		
		if type(value) in [int, long, Decimal, float]:
			self.digits = [Decimal(str(value)) for i in range(0, self.max_digits)]
		elif type(value) in [tuple, list]:
			self.digits = list(value)
		elif callable(value):
			self.calc_digit = value
			
		elif value == "add":
			self.calc_digit = self.calc_digit_added
		
		elif value == "subtract":
			self.calc_digit = self.calc_digit_sub
		
		elif value == "mult":
			self.calc_digit = self.calc_digit_mult
		
		elif value == "div":
			self.calc_digit = self.calc_digit_div

	def calc_digit_div(self, place):
		return Decimal(self.x_parent[place]) / Decimal(self.y_parent[place]) 


	def calc_digit_mult(self, place):
		return Decimal(self.x_parent[place] * self.y_parent[place])

	def calc_digit_sub(self, place):
		return Decimal(self.x_parent[place] - self.y_parent[place])
			
	def calc_digit_added(self, place):
		return Decimal(self.x_parent[place] + self.y_parent[place])
	
	def __getitem__(self, digit):
		if digit > self.max_digits:
			digit = self.max_digits
		
		return self.get_digit(digit)
			
	def get_digit(self, digit):
		"""request the hyperreal number's value at a specific place
		
		*needs error handling for if place exceeds self.max_digits 
		"""
		if digit < 0:
			digit = 0
		
		if len(self.digits) < digit+1 or digit == 0:
			value = self.calc_digit(digit)
			self.digits.append(value)

		else:
			value = self.digits[digit]
				
		return(value)
		
		
 	def calc_digit(self, digit):
 		"""
 		IMPORTANT__
 		calc_digit should only calculate the current digit, and return it.
 		It should not change self.digits directly (that's done internally).
 		
 		The "calc_digit" function should implement a hyperreal number that:
 			_is monotonic (or negative monotonic)
 			_abides by any other requirements a free filter would require on the operators.
 			_i.e. this function should not generate digits that will confuse:
 				_the standard function
 				_the equality function
 				_the greater than/less than function(s)
 			_this function *can still* ignore all of those constraints (and that would probably break the operators _ so be careful! use at your own risk!)
 			
 		This is a generic calc_digit. It assumes every place of the hyperreal number is 		constant. 
 		
 		This is the function to override to make interesting hyperreal numbers, 
 		like the infitestimal or the infinite hyperreal numbers.
 		"""
 		return(self.digits[0])
 
	def __le__(x, y, rounds=15, max_digits=30):
		"""
		_DON'T TEST EQUIVALENCE BETWEEN REAL NUMBERS AND HYPERREAL NUMBERS _ will always result in NotImplemented
		(maybe change that later)
		
		returns True if every digit is equal between the two
		returns False otherwise
		
		"""
		if type(x) != Hyperreal or type(y) != Hyperreal:
			return NotImplemented
		
		result = True
		for round in range(0,rounds):
			digit = random.randint(0,max_digits - 1)
			if x[digit] > y[digit]:
				result = False
		
		return result

	def __eq__(self,other, rounds=15, max_digits=30):
		"""
		_DON'T TEST EQUIVALENCE BETWEEN REAL NUMBERS AND HYPERREAL NUMBERS _ will always result in NotImplemented
		(maybe change that later)
		
		returns True if every digit is equal between the two
		returns False otherwise
		
		"""
		if type(self) != Hyperreal or type(other) != Hyperreal:
			return NotImplemented
		
		result = True
		for round in range(0,rounds):
			digit = random.randint(0,max_digits - 1)
			if self[digit] != other[digit]:
				result = False
		
		return result
	#In python 3.2 and above, the following are just @total_ordering
	
	def __ne__(self, other, rounds=15, max_digits=30):
		return (not self == other)
	
	def __lt__(self, other, rounds=15, max_digits=30):
		return( self <= other and (self != other) )
	
	def __gt__(self, other, rounds=15, max_digits=30):
		return(not self <= other)
	
	def __ge__(self, other, rounds=15, max_digits=30):
		return(not self < other) 
 	
 	def __len__(self):
 		return self.max_digits
	def std(self):
		"""This implements something akin to the standard operator.
		For the time being, it will just return the last digit of the hyperreal number.
		In the future, it would be good to do something more sophisticated
		(i.e. some sort of limit approximation routine)
		"""
		return(self.get_digit(self.max_digits) )
	
	#use std to define __float__, __int__ and such
	def __float__(self):
		return float(self.std())
	
	def __int__(self):
		return int(self.std())
	
	def __long__(self):
		return long(self.std())

	def __div__(self, y):
			if type(y) != Hyperreal:
					y = Hyperreal(y)
			
			return(Hyperreal("div", self, x_parent=self, y_parent=y))

	def __mul__(self, y):
			if type(y) != Hyperreal:
					y = Hyperreal(y)
			
			return(Hyperreal("mult", self, x_parent=self, y_parent=y))
	
	def __sub__(self, y):
			if type(y) != Hyperreal:
					y = Hyperreal(y)
			
			return(Hyperreal("subtract", self, x_parent=self, y_parent=y))
			
	def __add__(self, y):
			if type(y) != Hyperreal:
					y = Hyperreal(y)                
			add_result = Hyperreal("add", x_parent = self, y_parent = y)
			return add_result	

	def __repr__(self):
 		return str(self.digits)

if __name__ == "__main__":
	
	h_number = Hyperreal(3.15)
	y_number = Hyperreal(3.14)
	epsilon = Hyperreal(lambda digit: Decimal(1)/Decimal(digit+1) ) #my first infinitesimal
	epsilon2 = Hyperreal(lambda digit: Decimal(1)/Decimal(2**digit) )
	print h_number== y_number
	print h_number != h_number
	print y_number <= y_number
	
	y = (h_number - y_number)
	print y[4]
	print y.std()
	print float(y)
	print int(y)
	print long(y)
	print "Now to print the separate 'digits' of epsilon2."
	for digit in range(0,len(epsilon2)+1):
		print "The %sth digit is %s" % ( str(digit), str(epsilon2[digit]),) 
	print "The standard part of epsilon2 is %s" % (str(epsilon2.std())) 
	
	#print h_number[4]
